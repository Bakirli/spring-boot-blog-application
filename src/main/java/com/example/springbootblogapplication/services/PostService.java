package com.example.springbootblogapplication.services;

import com.example.springbootblogapplication.models.Post;
import com.example.springbootblogapplication.repositories.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PostService {

    private final PostRepository repository;

    public Optional<Post> getById(Long id) {
        return repository.findById(id);
    }

    public List<Post> getAll() {
        return repository.findAll();
    }

    public Post save(Post post) {
        if (post.getId() == null){
            post.setCreatedAt(LocalDateTime.now());
        }
        return repository.save(post);
    }
}
