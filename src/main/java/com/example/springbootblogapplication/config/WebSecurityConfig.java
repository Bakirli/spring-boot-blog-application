package com.example.springbootblogapplication.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.springframework.boot.autoconfigure.security.servlet.PathRequest.toH2Console;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig {

    @Bean
    public static PasswordEncoder passwordEncoder (){
        return new BCryptPasswordEncoder();
    }

    private final static String[] WHITELIST = {
            "/",
            "/register"
    };

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {

//        httpSecurity.csrf().disable()
//                .headers().frameOptions().disable()
//                .and().authorizeHttpRequests().requestMatchers(WHITELIST).permitAll()
//                .and().authorizeHttpRequests().requestMatchers(toH2Console()).permitAll()
//                .and().authorizeHttpRequests().requestMatchers(HttpMethod.GET,"/posts/*").permitAll();

        httpSecurity.csrf().disable()
                        .headers().disable()
                        .authorizeHttpRequests(auth -> auth.requestMatchers(WHITELIST).permitAll()
                                .requestMatchers(toH2Console()).permitAll()
                                .requestMatchers(HttpMethod.GET, "/posts/**").permitAll()
                                .anyRequest().authenticated()
                        );

            httpSecurity.formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/login")
                    .usernameParameter("email")
                    .passwordParameter("password")
                    .defaultSuccessUrl("/", true)
                    .failureUrl("/login?error")
                    .permitAll()
                    .and()
                    .logout()
                    .logoutUrl("/logout")
                    .logoutSuccessUrl("/login?logout")
                    .and()
                    .httpBasic();

        return httpSecurity.build();
    }
}
