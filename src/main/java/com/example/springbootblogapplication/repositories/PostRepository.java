package com.example.springbootblogapplication.repositories;

import com.example.springbootblogapplication.models.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {
}
