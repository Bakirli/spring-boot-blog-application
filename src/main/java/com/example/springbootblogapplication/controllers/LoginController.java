package com.example.springbootblogapplication.controllers;

import com.example.springbootblogapplication.services.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class LoginController {

    private final AccountService accountService;

    @GetMapping("login")
    public String login() {
        return "login";
    }
}
