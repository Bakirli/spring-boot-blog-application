package com.example.springbootblogapplication.controllers;

import com.example.springbootblogapplication.models.Account;
import com.example.springbootblogapplication.services.AccountService;
import com.example.springbootblogapplication.services.PostService;
import com.example.springbootblogapplication.models.Post;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;
    private final AccountService accountService;

    @GetMapping("posts/{id}")
    public String getPost(@PathVariable Long id, Model model) {
        Optional<Post> postOptional = postService.getById(id);
        if (postOptional.isPresent()) {
            Post post = postOptional.get();
            model.addAttribute("post", post);
            return "post";
        } else return "404";
    }

    @GetMapping("posts/new")
    public String newPost(Model model){
        Optional<Account> optionalAccount = accountService.findByEmail("hello@gmail.com");
        if (optionalAccount.isPresent()){
            Post post = new Post();
            post.setAccount(optionalAccount.get());
            model.addAttribute("post",post);
            return "newpost";
        } else return "404";
    }

    @PostMapping("posts/new")
    public String createNewPost(@ModelAttribute Post post){

        postService.save(post);

        return "redirect:/posts/" + post.getId();
    }

    @GetMapping("/posts/{id}/edit")
//    @PreAuthorize("isAuthenticated()")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getPostForEdit(@PathVariable Long id, Model model) {
        Optional<Post> optionalPost = postService.getById(id);
        if (optionalPost.isPresent()){
            Post post = optionalPost.get();
            model.addAttribute("post", post);
            return "post_edit";
        } else return "404";
    }

    @PostMapping("/posts/{id}")
    @PreAuthorize("isAuthenticated()")
    public String updatePost(@PathVariable Long id, Post post, BindingResult bindingResult, Model model) {
        Optional<Post> optionalPost = postService.getById(id);
        if (optionalPost.isPresent()) {
            Post existingPost = optionalPost.get();

             existingPost.setTitle(post.getTitle());
             existingPost.setBody(post.getBody());

             postService.save(existingPost);
        }

        return "redirect:/posts/" + post.getId();
    }
}
