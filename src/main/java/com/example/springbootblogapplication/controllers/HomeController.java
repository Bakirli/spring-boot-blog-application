package com.example.springbootblogapplication.controllers;

import com.example.springbootblogapplication.services.PostService;
import com.example.springbootblogapplication.models.Post;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class HomeController {

    private final PostService postService;

    @GetMapping("/")
    public String home(Model model) {
        List<Post> postList = postService.getAll();
        model.addAttribute("posts", postList);
        return "home";

    }
}
